mandelbrot-prelude
==================

Exploring the Mandelbrot set.


Usage
-----

```
git clone https://code.mathr.co.uk/mandelbrot-graphics.git
git clone https://code.mathr.co.uk/mandelbrot-numerics.git
git clone https://code.mathr.co.uk/mandelbrot-symbolics.git
git clone https://code.mathr.co.uk/mandelbrot-text.git
git clone https://code.mathr.co.uk/mandelbrot-prelude.git
cd mandelbrot-prelude
cabal repl --ghc-options "-interactive-print pprint"  # for ghc
./hugs.sh  # for hugs
```
