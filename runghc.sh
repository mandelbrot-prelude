#!/bin/sh
me="$(dirname "$(readlink -e "${0}")")"
runghc -package mtl -package parsec -i"${me}/hs/lib":"${me}/../mandelbrot-symbolics/hs/lib":"${me}/../mandelbrot-text/hs/lib":"${me}/../mandelbrot-text/hs/lib/ghc":"${me}/../mandelbrot-numerics/hs/lib":"${me}/../mandelbrot-graphics/hs/lib" "$@"
