#!/bin/sh
me="$(dirname "$(readlink -e "${0}")")"
runhugs -98 -E"nano -l -q +%d %s" -p"> " -P:"${me}/hs/lib":"${me}/../mandelbrot-symbolics/hs/lib":"${me}/../mandelbrot-text/hs/lib":"${me}/../mandelbrot-text/hs/lib/hugs":"${me}/../mandelbrot-numerics/hs/lib":"${me}/../mandelbrot-graphics/hs/lib" "${@}"
