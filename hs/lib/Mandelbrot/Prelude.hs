module Mandelbrot.Prelude
  ( module Mandelbrot.Graphics
  , module Mandelbrot.Numerics
  , module Mandelbrot.Symbolics
  , module Mandelbrot.Text
  , addressShape
  ) where

import Mandelbrot.Graphics
import Mandelbrot.Numerics hiding
  ( double
  )
import Mandelbrot.Symbolics hiding
  ( (!)
  , (!<)
  , concat
  , concatMap
  , drop
  , splitAt
  , take
  , Rational(..)
  )
import Mandelbrot.Text

addressShape :: AngledAddress -> Shape
addressShape (Unangled _) = Cardioid
addressShape (Angled p1 t (Unangled p2))
  | p1 * fromInteger (denominator t) == p2 = Circle
  | otherwise = Cardioid
addressShape (Angled _ _ a) = addressShape a
