{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Main where

import Mandelbrot.Prelude hiding (double)
import Mandelbrot.Numerics (double)

{-
import Numeric.Rounded

instance Plain R where
  plain = show
  plainPrec = showsPrec
  plainList = showList

instance Approx R where
  notFinite = notFiniteRealFloat
  bashZero = bashZeroNumEq
  approxEq = approxEqRealFloat

instance Square R where
  sqr x = x * x
  double x = x + x

type R = Rounded TowardNearest 256
-}
type R = Double
type C = Complex R

conjecture :: Int -> AngledAddress -> (ExternalAngle, ExternalAngle) -> Maybe (Maybe C, Maybe C)
conjecture p a (lo, hi)
  | addressShape a == Circle = Nothing
  | otherwise = case (locate' p lo, locate' p hi) of
     (Just clo, Just chi) | approxEq 8 clo chi -> Nothing
     p -> Just p

locate' :: Int -> ExternalAngle -> Maybe C
locate' p t = do
  e <- headMay . drop (factor * p * sharpness) . exRayIn sharpness . fromQ $ t
  nucleus' p e

factor :: Int
factor = 3

sharpness :: Int
sharpness = 8

headMay [] = Nothing
headMay (x:_) = Just x

enumerate =
  [ ((a, (lo, hi)), conjecture p a (lo, hi))
  | p <- [1..]
  , let d = 2^p - 1
  , n <- [0..d]
  , let t = n % d
  , period t == p
  , let a = angledAddress t
  , let (lo, hi) = addressAngles a
  , t == lo
  ]

output :: [((AngledAddress, (ExternalAngle, ExternalAngle)), Maybe (Maybe C, Maybe C))] -> IO ()
output ((r@(a, (lo, hi)), Just p):_) = do
  pprint (r, p)
  print . length . take (factor * period a * sharpness) . (id :: [C] -> [C]) . exRayIn sharpness . fromQ $ lo
  print . length . take (factor * period a * sharpness) . (id :: [C] -> [C]) . exRayIn sharpness . fromQ $ hi
output ((a, Nothing):es) = pprint a >> output es

main :: IO ()
main = output enumerate
