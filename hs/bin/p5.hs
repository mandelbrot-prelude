import Mandelbrot.Prelude

main = sequence_
  [ putImage c r 1000 >>
    pprint a >>
    pprint (binary lo) >>
    pprint (binary hi) >>
    putStrLn ""
  | let p = 5
  , let d = 2^p - 1
  , n <- [ 1 .. d - 1 ]
  , let t = n % d :: ExternalAngle
  , period t == p
  , let a = angledAddress t
  , let (lo, hi) = addressAngles a
  , t == lo
  , let sharpness = 8
  , let e = exRayIn sharpness (fromQ t) !! (2 * sharpness * p)
  , let c = nucleus p e
  , let r = 2 * magnitude (size p c)
  ]
