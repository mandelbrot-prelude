import Mandelbrot.Prelude

main :: IO ()
main = sequence_
  [ putStrLn $ unwords [ show p, plain a, plain (binary lo), plain (binary hi) ]
  | p <- [1..]
  , let d = 2^p - 1
  , n <- [0..d]
  , let t = n % d
  , period t == p
  , let a = angledAddress t
  , addressShape a == Cardioid
  , let (lo, hi) = addressAngles a
  , t == lo
  ]
